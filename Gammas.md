# Гаммы

## Минорная

![image](http://www.solfo.ru/images/minornaya_gamma.png?crc=4260125598 "Построение минорной гамммы Ре-минор")

Минорная гамма `<N>-минор` строится так:
берётся нота `<N>` и от неё берутся ноты в следующем порядке:

    тон-полутон-тон-тон-полутон-тон-тон

где `тон` - два лада, `полутон` - один лад.

Таким образом для гаммы Ре-минор получается:

    D-E-F-G-A-Bb-C-D

или:

    Ре-Ми-Фа-Соль-Ля-СиБемоль-До-Ре

Аналогично можно построить гамму До-минор:

    C-D-Eb-F-G-Ab-Bb-C
    До-Ре-МиБемоль-Фа-Соль-ЛяБемоль-СиБемоль-До

# Табулатуры некоторых гамм

## Гамма Ре-минор (Dm)

Ключевые точки (нота D (Ре)): 4с0л, 2с3л, 1с8л

    |---------------------------0-1-3-5-6-8-10-------------------
    |-----------------------1-3----------------------------------
    |-----------------0-2-3--------------------------------------
    |-----------0-2-3--------------------------------------------
    |-----0-1-3--------------------------------------------------
    |-1-3--------------------------------------------------------

## Гамма Ля-минор (Am)

Ключевые точки (нота A (Ля)): 5с0л, 3с2л, 1с5л

    |-----------------------------0-1-3-5-7-8----------------------
    |-----------------------0-1-3----------------------------------
    |-------------------0-2----------------------------------------
    |-------------0-2-3--------------------------------------------
    |-------0-2-3--------------------------------------------------
    |-0-1-3--------------------------------------------------------
